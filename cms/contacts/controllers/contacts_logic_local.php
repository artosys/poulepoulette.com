<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacts_logic_local extends Contacts_logic {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('contacts/contact');
		$this->load->model('contacts/contact_settings');
		$this->load->model('contacts/contact_types');
		$this->load->model('products/product');
		$this->load->library('email');
	}
	
	public function index()
	{
		redirect('');
	}
	
	public function show_form($par = "direct", $form = "none")
	{
		prepare_client_uploads(WEBSITE_KEYWORD);
		
		$types		= $this->contact_types->get_all();
		$types		= $this->contact_types->primary_indexed($types);
		$settings = $this->contact_settings->get_by('website_id', $this->client->current->website->id);
		$this->contact->order_by("sequence", "asc");
		if($form == "none")
			$fields	= $this->contact->get_many_by(array('website_id' => $this->client->current->website->id, 'form' => ''));
		else
			$fields	= $this->contact->get_many_by(array('website_id' => $this->client->current->website->id, 'form' => $form));
		$success_message = "";
		$error_message = "";

		// Overwritten from field_types?
		foreach($fields as $i => $field)
		{
			if(isset($types[$field->type_id]))
			{
				if($field->required == 0)
				{
					$fields[$i]->required = $types[$field->type_id]->required;
				}
				if($field->label == "")
				{
					$fields[$i]->label = $types[$field->type_id]->label;
				}
			}
			$fields[$i]->type = $types[$field->type_id]->type;
			
			switch($fields[$i]->type)
			{
				case "text":			$fields[$i]->value = ""; break;
				case "textarea":		
					if(!$this->input->post('btn_submit') && $this->input->get('tav'))
						$fields[$i]->value = "T.a.v. ".strip_tags($this->input->get('tav')); 
					else
						$fields[$i]->value = ""; 
					break;
				case "checkbox":		$fields[$i]->value = 0; break;
				case "autocomplete":	$fields[$i]->value = ""; break;
				case "radio":			$fields[$i]->value = 0; break;
			}
		}

		// Handle posted form
		if($this->input->post('btn_submit'))
		{
			$this->email->from("info@cubro.be", "Cubro");
			$do_handling = true;
			foreach($fields as $i => $field)
			{
				$posted_value = $this->input->post($this->contact->form_name($field->label)) ? $this->input->post($this->contact->form_name($field->label)) : "";
				if($field->type == "file")
				{
					if($_FILES[$this->contact->form_name($field->label)]['size'] != 0 && $_FILES[$this->contact->form_name($field->label)]['error'] == 0)
					{
						$result = $this->contact->upload_file($_FILES[$this->contact->form_name($field->label)], $types[$field->type_id]->regex);
						if(!$result["success"])
						{
							$do_handling = false;
							$error_message = $result["error"];
						}
						else
						{
							$field->url = SHAREURL."uploads/".WEBSITE_KEYWORD.$result['path'];
						}
					}
					else
					{
						$field->url = "";
					}
				}
				else
				{
					if($field->required == 2 && trim($posted_value) == "")
					{
						$do_handling = false;
						$error_message = "Gelieve alle verplichte velden in te vullen.";
					}
					if($types[$field->type_id]->type != "radio" && $types[$field->type_id]->regex != "" && !preg_match("/".$types[$field->type_id]->regex."/", $posted_value))
					{
						$do_handling = false;
						$error_message = "Gelieve geldige waardes in te geven.";
					}
					$fields[$i]->value = strip_tags($posted_value);
					
					// CUSTOM CHECK OBV. VAN SELECTBOX
					if( $field->label == 'Afdeling' )
					{
						if ( trim($posted_value) == "Finance" ) { $this->email->to('FINANCE@POULEPOULETTE.COM'); }
						elseif ( trim($posted_value) == "Marketing" ) { $this->email->to('MARKETING@POULEPOULETTE.COM'); }
						elseif ( trim($posted_value) == "Aankoop" ) { $this->email->to('AANKOOP@POULEPOULETTE.COM'); }
						elseif ( trim($posted_value) == "Legal" ) { $this->email->to('LEGAL@POULEPOULETTE.COM'); }
						elseif ( trim($posted_value) == "Jobs" ) { $this->email->to('JOBS@POULEPOULETTE.COM'); }
						elseif ( trim($posted_value) == "Franchise" ) { $this->email->to('FRANCHISE@POULEPOULETTE.COM'); }
						elseif ( trim($posted_value) == "Admin" ) { $this->email->to('ADMIN@POULEPOULETTE.COM'); }
						else { $this->email->to($settings->email); }
					}
					
					// CUSTOM CHECK OBV. VAN SELECTBOX
					if( $field->label == 'Shop' )
					{
						if ( trim($posted_value) == "Brasschaat" ) { $this->email->to('rugen@artosys.be'); }
						elseif ( trim($posted_value) == "Wijngaardbrug" ) { $this->email->to('rugen@cubro.be'); }
						else { $this->email->to($settings->email); }
					}
					
					// CUSTOM CHECK OBV. VAN CHECKBOX
					if( $field->label == 'Kip versneden' )
					{
						if ( trim($posted_value) === true || trim($posted_value) == 'on' ) { $fields[$i]->value = 'Ja'; }
						else { $fields[$i]->value = 'Nee'; }
					}
					
					// CUSTOM CHECK OBV. VAN CHECKBOX
					if( $field->label == 'Jus op de kip' )
					{
						if ( trim($posted_value) === true || trim($posted_value) == 'on' ) { $fields[$i]->value = 'Ja'; }
						else { $fields[$i]->value = 'Nee'; }
					}
					
					if($field->is_reply_to == "1")
					{
						$this->email->from($fields[$i]->value, "Website contact");
					}
				}
			}
			
			if($do_handling)
			{
				//$this->email->to($settings->email);
				
				if( $form == 'order' ) { $this->email->subject('Website bestelling'); }
				else { $this->email->subject('Website contact'); }
				
				$mail_message = 
					'<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.=w3.org/TR/html4/loose.dtd">
					<html>
					<head>
					   <title>Website contact</title>
					   <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
					   <style type="text/css">
							body {
							   width: 100% !important; color: #333333 !important; background: #ffffff;
							   font: 13px Helvetica, Arial, Verdana, sans-serif;
						   }
						   h1 {
							   font: 20px Helvetica, Arial, Verdana, sans-serif;
							   font-weight: bold; color: #555555; padding-bottom: 15px !important;
						   }
						   h2 {
							   font: 16px Helvetica, Arial, Verdana, sans-serif !important; margin-bottom: 1px !important;
							   font-weight: bold !important; color: #EC855E !important; padding-bottom: 5px !important; padding-top: 20px !important;
						   }
						   table { width: 600px; }
						   .hoofd { background: #f0f0f0; border-bottom: 1px solid #ddd; }
					   </style>
					</head>
					<body style="width: 100% !important; color: #333333 !important; background: #ffffff; font: 13px Helvetica, Arial, Verdana, sans-serif;">';
				
				$body_html = 
						'<h1 style="font: 20px Helvetica, Arial, Verdana, sans-serif; font-weight: bold; color: #555555; padding-bottom: 15px !important;">Contact via uw website</h1>
						Zonet heeft een bezoeker het contactformulier gebruikt op '.$this->client->current->website->url.' om een bericht te sturen.<br/>
						Onderstaand ziet u de ingevulgde gegevens en het eigenlijke bericht van de bezoeker:<br/><br/>
						<table style="width: 600px;" width="600">';
				$body_txt = 
						"Contact via uw website\r\n\r\n
						Zonet heeft een bezoeker het contactformulier gebruikt op ".$this->client->current->website->url." om een bericht te sturen.\r\n
						Onderstaand ziet u de ingevulgde gegevens en het eigenlijke bericht van de bezoeker.\r\n\r\n";
				foreach($fields as $i => $field)
				{
					if($field->type == "file")
					{
						$body_html .= '<tr><td style="background: #f0f0f0; border-bottom: 1px solid #ddd;">'.$field->label.': &nbsp;&nbsp;</td></tr><tr><td><a href="'.$field->url.'">'.$field->url.'</a></td></tr>';
						$body_txt .= $field->label.": ".$field->url."\r\n\r\n";
					}
					elseif($field->type == "autocomplete")
					{
						$body_html .= '<tr><td style="background: #f0f0f0; border-bottom: 1px solid #ddd;">'.$field->label.': &nbsp;&nbsp;</td></tr><tr><td>';
						$body_txt .= $field->label.": ";
						foreach(explode('##', $field->value) as $prod)
						{
							$body_html .= strip_tags($prod)."<br/>";
							$body_txt .= strip_tags($prod)."\r\n";
						}
						$body_html .= '</td></tr>';
						$body_txt .= "\r\n";
					}
					else
					{
						$body_html .= '<tr><td style="background: #f0f0f0; border-bottom: 1px solid #ddd;">'.$field->label.': &nbsp;&nbsp;</td></tr><tr><td>'.nl2br($field->value).'</td></tr>';
						$body_txt .= $field->label.": ".$field->value."\r\n\r\n";
					}						
				}
				
				$mail_message .= $body_html;
				$mail_message .= '</table><br/><br/>Met vriendelijke groeten,<br/>Cubro Systeem';
				$mail_message = str_replace("<br />","<br>",$mail_message);
				$mail_message = str_replace("&nbsp;","",$mail_message);
				$mail_message = str_replace("&#39;","'",$mail_message);     
				$this->email->message($mail_message);
				$this->email->set_alt_message($body_txt);

				$this->email->mailtype = "html";
				$this->email->send();
				
				$success_message = "Uw bericht werd succesvol verstuurd.";
				
				foreach($fields as $i => $field)
				{
					switch($fields[$i]->type)
					{
						case "text":		$fields[$i]->value = ""; break;
						case "textarea":	$fields[$i]->value = ""; break;
						case "checkbox":	$fields[$i]->value = 0; break;
						case "radio":		$fields[$i]->value = 0; break;
					}
				}
			}
		}
		
		$this->page_data['success_message']	= $success_message;
		$this->page_data['error_message']	= $error_message;
		$this->page_data['fields']			= $fields;
		$this->page_data['types']			= $types;
		$this->page_data['settings']		= $settings;

		// Determine layout
		$view = "contacts/contact";
		if(file_exists("views/contacts/contact_".$form.".php"))
		{
			$view = "views/contacts/contact_".$form.".php";
		}
		
		// Determine output manner
		if($par == "partial")
			$this->layout->partial($view, TRUE, $this->page_data);
		else
			$this->layout->content($view, $this->page_data);
	}
	
	public function ajax_products()
	{
		$term = $this->input->get('term');
		
		echo json_encode($this->product->complete($term,10));
	}

}