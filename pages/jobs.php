				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
                
				<div id="display_0" class="contentsection gray" style="border-top: none;">
                    <div class="mcontent">
						<div class="inner clearfix">
        				    <div class="divider-empty"></div>
							<h1 class="text-center" style="margin-bottom: 60px;">Jobs</h1>
							<div class="job-info baconfont">
							<p>Poule &amp; Poulette is permanent op zoek naar leuke &amp; vriendelijke medewerkers!</p>
							
							<p>Passie voor good food is een must.<br /> Indien je dan nog eens een gezonde motivatie hebt dan maken wij van u een topper !</p>
							
							<p>Je mag uw cv sturen naar onderstaande link : </p>
							
							<p>
								<script type="text/javascript" language="javascript">
									{ coded = "oMzK@fMQWqfMQWqssq.lMR"
									  key = "rPNmAnZbKe6SM5WE0yfFGxDkgRt78zjqc1wBJv2uHXY9IU3TsahdoLlVi4pCOQ"
									  shift=coded.length
									  link=""
									  for (i=0; i<coded.length; i++) {
										if (key.indexOf(coded.charAt(i))==-1) {
										  ltr = coded.charAt(i)
										  link += (ltr)
										}
										else {     
										  ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length
										  link += (key.charAt(ltr))
										}
									  }
									document.write("<a href='mailto:"+link+"'>"+link+"</a>")
									}
								</script><noscript>Sorry, you need Javascript on to email me.</noscript>
							</p>
							
							<p>Succes!</p>
							<p>Welkom @ de Poule &amp; Poulette Familie !</p>
							</div>
                        </div>
                    </div>
				</div>
				
				<div id="display_1" class="contentsection gray">
					<div class="mcontent">
                        <div class="divider"></div>
                        <h2 class="text-center">Locale jobs</h2>
                        <p class="text-center baconfont">Bekijk ook regelmatig het nieuws van onze lokale vestigingen voor jobs in uw regio.</p>
                        <div class="row">
							
							<?php
								$stores = Modules::run('stores/stores_logic/get_stores'); 
								foreach($stores as $store){
							?>
								<div class="col-md-4">
                                        <div class="apply" style="margin: 0 auto;">
    										<div class="wbutton">
    											<a href="{url}stores/view/<?= $store->id ?>/<?= urlencode($store->name) ?>#Nieuws" style="color: #fff; text-decoration: none; text-align: center;">Nieuws <?= $store->name ?></a>
    										</div>
    									</div>
								</div>
							<?php 
								} 
							?>
                        </div>
                    </div>
                </div>