				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
				
				<div id="display_0" class="contentsection gray">
                    <div class="mcontent">
						<div class="inner clearfix">
        				    <div class="divider-empty"></div>
                            <h1 class="text-center" style="margin-bottom: 80px;">Menukaart</h1>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="menu-box image menu-card text-center">
                                        <h3 class="text-center">Resto</h3>
                                        
                                        <a rel="gallery" title="Title 1" href="{url}images/menu/Poule-Poulette-Menukaart-shop.jpg">
                                            <img src="{url}images/menu/Poule-Poulette-Menukaart-shop.jpg">
                                        </a>
                                        
                                        <a href="{url}images/files/MENUKAART SHOP.pdf" target="_blank" class="read-more">Afdrukken</a>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="menu-box image menu-card text-center">
                                        <h3 class="text-center">Shop</h3>
                                        
                                        <a rel="gallery" title="Title 1" href="{url}images/menu/Poule-Poulette-Menukaart-shop.jpg">
                                            <img src="{url}images/menu/Poule-Poulette-Menukaart-shop.jpg">
                                        </a>
                                        
                                        <a href="{url}images/files/MENUKAART SHOP.pdf" target="_blank" class="read-more">Afdrukken</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
				</div>
                
                <div id="display_1" class="contentsection gray" style="padding-bottom: 0;">
					<div class="mcontent">
						<div class="inner clearfix">
							<div class="divider"></div>
                            <div id="imagegallery2">
								<div id="gallerystage" class="flexslider">
									<ul class="slides">
										<li><img src="{url}images/slider/menu-01.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-02.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-03.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-04.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-05.png" alt="galleryimage"></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>