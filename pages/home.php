				<div id="feature" class="flexslider">
                    <div class="top-area">
                       <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
						<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                    </div>
                    
					<ul class="slides">
						<li>
							<img class="slideimage" src="{url}images/slider/home-02.jpg" alt="slideshow">
							<div class="caption">
								<div class="mcontent">
									<div class="captioncontent">
										<h2>Slow Cooking op unieke houtoven</h2>
									</div>
								</div>
							</div>
						</li>
						<li>
							<img class="slideimage" src="{url}images/slider/home-04.jpg" alt="slideimage">
							<div class="caption">
								<div class="mcontent">
									<div class="captioncontent">
										<h2>Resto : Gezellig, Lekker &amp; Trendy</h2>
									</div>
								</div>
							</div>
						</li>
						<li>
							<img class="slideimage" src="{url}images/slider/home-01.jpg" alt="slideimage">
							<div class="caption">
								<div class="mcontent">
									<div class="captioncontent">
										<h2>De lekkerste gebraden kippen</h2>
									</div>
								</div>
							</div>
						</li>
						<li>
							<img class="slideimage" src="{url}images/slider/home-03.jpg" alt="slideimage">
							<div class="caption">
								<div class="mcontent">
									<div class="captioncontent">
										<h2>Shop : Snel, Lekker &amp; Gezond</h2>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				
				<div id="display_0" class="contentsection gray">
					<div class="mcontent">
						<div class="inner clearfix">
							<div class="divider-empty"></div>
							<div class="text-center">
								<img src="{url}images/pp-naam.png" style="max-width: 90%;" />
							</div>
							<div class="divider-empty"></div>
                            <h1>Ontdek onze website</h1>
                        
							<div class="clearfix row">
								
								<div class="col-md-4 nav-box-container">
    								<div class="nav-box">
                                        <div class="circle-icon" id="icn-menu"></div>
    									<h2>Menukaart</h2>
    									
    									<div class="apply">
    										<div class="wbutton">
    											<a href="{url}kaart">Bekijken</a>
    										</div>
    									</div>
    								</div>  
                                </div>
								<div class="col-md-4 nav-box-container">
    								<div class="nav-box">
                                        <div class="circle-icon" id="icn-locaties"></div>
    									<h2>Locaties</h2>
    									
    									<div class="apply">
    										<div class="wbutton">
    											<a href="{url}#Locations">Bekijken</a>
    										</div>
    									</div>
    								</div> 
                                </div>
                                <div class="col-md-4 nav-box-container">
    								<div class="nav-box">
                                        <div class="circle-icon" id="icn-reserveer"></div>
    									<h2>Reserveer online</h2>
    									
    									<div class="apply">
    										<div class="wbutton">
    											<a href="{url}reserveer">Bekijken</a>
    										</div>
    									</div>
    								</div> 
                                </div>
                                <div class="col-md-4 nav-box-container">
    								<div class="nav-box">
                                        <div class="circle-icon" id="icn-kip"></div>
    									<h2>Poule &amp; Poulette</h2>
    									
    									<div class="apply">
    										<div class="wbutton">
    											<a href="{url}poulepoulette">Bekijken</a>
    										</div>
    									</div>
    								</div>
                                </div>
                                <div class="col-md-4 nav-box-container">
    								<div class="nav-box">
                                        <div class="circle-icon" id="icn-jobs"></div>
    									<h2>Jobs</h2>
    									
    									<div class="apply">
    										<div class="wbutton">
    											<a href="{url}jobs">Bekijken</a>
    										</div>
    									</div>
    								</div>
                                </div>
                                <div class="col-md-4 nav-box-container">
    								<div class="nav-box">
                                        <div class="circle-icon" id="icn-franchise"></div>
    									<h2>Franchise</h2>
    									
    									<div class="apply">
    										<div class="wbutton">
    											<a href="{url}franchise">Bekijken</a>
    										</div>
    									</div>
    								</div>
                                </div>
							</div>
						</div>
					</div>
				</div>
                
                <div id="display_1" class="contentsection normal">
					<div class="mcontent">
						<div class="inner clearfix">
                        
                            <div class="divider"></div>
                            <h1>Laatste nieuws</h1>
							
								<div class="row" style="text-align: center; overflow: hidden;">
                                
                                        <div class="apply" style="width: 400px; max-width: 90%; margin: 0 auto;">
    										<div class="wbutton">
    											<a href="https://www.facebook.com/poulepoulette.belgium" target="_blank" style="color: #fff; position: relative; text-decoration: none;"><i class="fa fa-thumbs-up"></i>Volg ons op Facebook</a>
    										</div>
    									</div>
                                
                                </div>
							
							<div class="clearfix">
								
								
								<?= Modules::run('posts/posts_logic/index', 'partial', 3, '', 'homepage_view'); ?>	
								
                                
                                <div class="clearfix"></div>
                                
                                <div class="row" style="text-align: center; overflow: hidden;">
                                
                                        <div class="apply" style="width: 400px; max-width: 90%; margin: 0 auto;">
    										<div class="wbutton">
    											<a href="{url}nieuws" style="color: #fff; text-decoration: none;">Meer nieuws</a>
    										</div>
    									</div>
                                
                                </div>
                                
							</div>
						</div>
					</div>
				</div>

				
				<a id="Locations"></a>
				<div id="content" class="contentsection normal" style="">
					<div id="display_2" class="mcontent" style="background-color: #000;">
							
							<div class="divider"></div>
                            <h1 class="text-center">Locaties</h1>
							<div class="divider-empty"></div>
						
							<div class="boxedmap">
								<div class="sndwchmap" data-zoom="11" data-lat="51.25128" data-long="4.4101375"></div>
							</div>
							<div class="clearfix">
								<?php
								$stores = Modules::run('stores/stores_logic/get_stores'); 
								foreach($stores as $store){
								?>
								<div class="locationitem" 
											<?php if ( $store->id == 4 ) { ?>
											data-lat="51.2880117" data-long="4.4872306"
											<?php } ?>
											<?php if ( $store->id == 5 ) { ?>
											data-lat="51.2184281" data-long="4.3968499"
											<?php } ?>
											<?php if ( $store->id == 6 ) { ?>
											data-lat="51.22128" data-long="4.4051375"
											<?php } ?>
									 >
									<h2><?= $store->name ?> <span></span></h2>
									<div class="locationinfo clearfix">
										<div class="address clearfix">
											<p class="adres-gegevens"><?= nl2br($store->address) ?></p>
										</div>
                                        <div class="tags clearfix">
											<?php if ( $store->id == 4 ) { ?>
											<span>Shop</span>
											<?php } ?>
											<?php if ( $store->id == 5 ) { ?>
											<span>Resto</span>
											<?php } ?>
											<?php if ( $store->id == 6 ) { ?>
											<span>Shop</span> + <span>Eetkamer</span>
											<?php } ?>
										</div>
                                        <a href="{url}stores/view/<?= $store->id ?>/<?= urlencode($store->name) ?>">Meer gegevens</a>
									</div>
								</div>	
								<?php 
								} 
								?>
							</div>
						
					</div>
				</div>

				
				<a id="Lifestyle"></a>
				<div id="display_3" class="contentsection gray last-section">
					<div class="mcontent">
						<div class="inner clearfix">
							<div class="divider"></div>
                            <div id="imagegallery">
								<div id="gallerystage" class="flexslider">
									<ul class="slides">
										<li><img src="{url}images/slider/bottom-01.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/bottom-02.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/bottom-03.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/bottom-04.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/bottom-05.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/bottom-06.png" alt="galleryimage"></li>
									</ul>
								</div>
								<img class="roid" src="images/pic6m.jpg" style="left:0px;" alt="galleryimage">
								<img class="roid" src="images/raviolim.jpg" style="right:0px;" alt="galleryimage">
							</div>
						</div>
					</div>
				</div>
