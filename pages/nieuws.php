			<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
                
                <div id="display_0" class="contentsection gray" style="border-top: none;">
					<div class="mcontent">
                        <div class="divider-empty"></div>
                        <h2 class="text-center">Bekijk ook het nieuws van onze lokale vestigingen</h2>
                        <div class="row">
                            <?php
								$stores = Modules::run('stores/stores_logic/get_stores'); 
								foreach($stores as $store){
							?>
								<div class="col-md-4">
                                        <div class="apply" style="margin: 0 auto;">
    										<div class="wbutton">
    											<a href="{url}stores/view/<?= $store->id ?>/<?= urlencode($store->name) ?>#Nieuws" style="color: #fff; text-decoration: none; text-align: center;">Nieuws <?= $store->name ?></a>
    										</div>
    									</div>
								</div>
							<?php 
								} 
							?>
                        </div>
                    </div>
                </div>
		
				<div id="display_1" class="contentsection gray">
					<div class="mcontent">
						<div class="divider"></div>
                        <h1 class="text-center">Nieuws</h1>
                        <div class="inner clearfix">
							
							<div class="row" style="text-align: center; overflow: hidden;">
                                
                                        <div class="apply" style="width: 50%; margin: 0 auto;">
    										<div class="wbutton">
    											<a href="https://www.facebook.com/poulepoulette.belgium" target="_blank" style="color: #fff; position: relative; text-decoration: none;"><i class="fa fa-thumbs-up"></i>Volg ons op Facebook</a>
    										</div>
    									</div>
                                
                           </div>
							
                            		
                            			
                            <?= Modules::run('posts/posts_logic/index', 'partial', 0, '', 'main_view'); ?>				
                       	
                            
						</div>
					</div>
				</div>