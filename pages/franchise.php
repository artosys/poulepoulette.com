				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
				<div id="display_0" class="contentsection gray" style="border-top: none;">
                    <div class="mcontent">
						<div class="inner clearfix">
							<div class="divider-empty"></div>
							<h1 class="text-center" style="margin-bottom: 60px;">Franchise</h1>
							
							<div class="job-info baconfont" style="margin-bottom: 80px;">
							<p>Ben je op zoek naar een nieuwe uitdaging?</p>
							<p>Contacteer ons voor onze franchise voorwaarden</p>
							</div>
							
							<div id="imagegallery2" style="width: 50%; margin: 0 auto;">
								<div id="gallerystage" class="flexslider">
									<ul class="slides">
										<li><img src="{url}images/slider/franchise-01.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/franchise-02.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/franchise-03.png" alt="galleryimage"></li>
									</ul>
								</div>
							</div>
							<div class="divider-empty"></div>
							<div class="divider-empty"></div>
							<div class="divider"></div>
							<h1 class="text-center" style="margin-bottom: 60px;">Historiek</h1>
							<div class="widget Blog history" id="Blog1">
								<div class="blog-posts hfeed clearfix" id="grid">
										<div class="animated">
                            				<div class="post hentry clearfix">
												<div class="post-body entry-content">
													<div class="date">September 2012</div>
													<h3 class="text-center">Opening Shop Brasschaat</h3>
												</div>
                            				</div>
                            			</div>	
										<div class="animated">
                            				<div class="post hentry clearfix">
												<div class="post-body entry-content">
													<div class="date">Augustus 2014</div>
													<h3 class="text-center">Opening Resto Antwerpen Sint Jansvliet</h3>
												</div>
                            				</div>
                            			</div>	
										<div class="animated">
                            				<div class="post hentry clearfix">
												<div class="post-body entry-content">
													<div class="date">December 2014</div>
													<h3 class="text-center">Heropening Shop Brasschaat</h3>
												</div>
                            				</div>
                            			</div>	
										<div class="animated">
                            				<div class="post hentry clearfix">
												<div class="post-body entry-content">
													<div class="date">April 2015</div>
													<h3 class="text-center">Opening Shop + Eetkamer Antwerpen Wijngaardbrug</h3>
												</div>
                            				</div>
                            			</div>	
								</div>
							</div>
				
                        </div>
                    </div>
				</div>
                
                <div id="display_1" class="contentsection normal">
					<div class="mcontent">
						<div class="inner clearfix">
							
							<div class="divider"></div>
                            <h1>Laatste nieuws</h1>
                        
							<div class="clearfix">
								
								<?= Modules::run('posts/posts_logic/index', 'partial', 3, '', 'homepage_view'); ?>	
                                
                                <div class="clearfix"></div>
                                
                                <div class="row" style="text-align: center; overflow: hidden;">
                                
                                        <div class="apply" style="width: 50%; margin: 0 auto;">
    										<div class="wbutton">
    											<a href="{url}nieuws" style="color: #fff; text-decoration: none;">Meer nieuws</a>
    										</div>
    									</div>
                                
                                </div>
                                
							</div>
						</div>
					</div>
				</div>