				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>

				<div id="display_0" class="contentsection gray">
                    <div class="mcontent">
                        <div class="inner clearfix">
							<div class="divider-empty"></div>
							<h1 class="text-center" style="margin-bottom: 80px;">Reserveer</h1>
                            
                            <div class="row">
								<div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="menu-box image menu-card text-center">
                                        <h3 class="text-center">Resto</h3>
                                        
                                        <div class="order-info">
                                            Wilt u zeker zijn dat u een plaatsje heeft in &eacute;&eacute;n van onze resto's?<br /><br />
                                            Reserveer dan uw tafel online.
                                        </div>
                                        
                                        <a href="#" class="read-more">Reserveer een tafel</a>
                                    </div>
                                </div>
								<div class="col-md-3"></div>
                                <!--
                                <div class="col-md-6">
                                    <div class="menu-box image menu-card text-center">
                                        <h3 class="text-center">Shop</h3>
                                        
                                        <div class="order-info">
                                            Geen tijd verliezen of zeker zijn van uw kip?<br /><br />
                                            Laat uw bestelling dan klaarzetten.
                                        </div>
                                        
                                        <a href="{url}order" class="read-more">Plaats een bestelling</a>
                                    </div>
                                </div>
								-->
                            </div>
                        </div>
                    </div>
				</div>
                
                <div id="display_1" class="contentsection gray" style="padding-bottom: 0;">
					<div class="mcontent">
						<div class="inner clearfix">
							<div class="divider"></div>
                            <div id="imagegallery2">
								<div id="gallerystage" class="flexslider">
									<ul class="slides">
										<li><img src="{url}images/slider/menu-01.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-02.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-03.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-04.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-05.png" alt="galleryimage"></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>