				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
				<div id="top-image">
                    
				</div>
				<div id="display_0" class="contentsection gray">
                    <div class="mcontent">
						<div class="inner clearfix">
        				    <div class="divider-empty"></div>
							
                            <h1 class="text-center" style="margin-bottom: 80px;">Het verhaal</h1>
							
							<img src="{url}images/team/team.jpg" style="float: right; margin: 0 25px 25px 25px; max-width: 350px;border: 10px solid white; border-bottom-width: 50px;" />
							<div class="job-info info-left">
								<h3>Ons product is onze passie</h3>
								<p>
								Kip wordt te weinig gewaardeerd als gezond en duurzaam alternatief voor vlees en vis. Poule &
								Poulette wil dat veranderen door van dit puur en mager product elke dag opnieuw iets heel lekker en gezond te maken. Niets zo veelzijdig dan een kip: koud of warm, krokant gebraden of sappig
								gebakken, lekker uit het vuistje of gezond en licht in een slaatje... aan inspiratie geen gebrek!
								</p>
							</div>
							<div class="job-info info-left">
								<h3>Focus op duurzaamheid en gezondheid</h3>
								<p>
								Alle Poule & Poulette- kippen komen uit Belgi&euml;, waardoor de herkomst steeds voor 100%
								traceerbaar is en het transport tot een minimum wordt beperkt.
								We gebruiken de volledige kip in diverse bereidingen, waardoor er quasi niets van het dier verloren gaat.
								Onze kippen voldoen aan ons eigen strenge kwaliteitslabel: geen antibiotica, plantaardige voeders, voldoende leefruimte, gekende vaste kwekers, 100 % natuurlijk en Belgische traceerbaarheid zorgen voor vetarm vlees van hoge kwaliteit met een rijkelijke smaak!
								Ook onze bijgerechten maken we met dezelfde focus op duurzaamheid en gezondheid klaar.
								Daarom bereiden we alle bijgerechten op ambachtelijke wijze, zonder toevoeging van
								bewaarmiddelen of stabilisatoren, dagvers.
								Van aperohapjes en soep tot bijgerechten en desserts: we selecteerden onze 30 nevenproducten
								met de grootste zorg zodat we je een volledige evenwichtige maaltijd met eerlijke ingrediënten rond kip kunnen aanbieden.
								De P&P-medewerkers delen onze passie voor lekker, gezond en eerlijk eten. Zij bereiden wekelijks meer dan duizenden kippen in onze trendy shops en resto(’s).
								</p>
							</div>
							<div class="job-info info-left">
								<h3>Visie</h3>
								<p>
								Poule & Poulette biedt kip-bereidingen aan volgens de “slowfood” fylosofie in een “goodfood” concept. Dit door op een snelle manier een volledige verse en ambachtelijke bereide maaltijd aan te bieden waarbij KIP centraal staat. Onze speciale bakwijze van kippen garandeert een artisanaal , gezond en smaakvol alternatief voor de bestaande fastfood mogelijkheden. Constante groei en verbetering van kwaliteit en processen zijn een dagdagelijkse focus in al onze acties en activiteiten. Door het samenwerken met mensen die willen presteren zullen wij onze gezonde groei naar meerdere filialen garanderen.
								</p>
							</div>
							<div class="job-info info-left">
								<h3>Ons verhaal en onze toekomstvisie</h3>
								<p>
								De 3 eigenaars van Poule & Poulette hebben verschillende achtergronden maar delen een passie voor lekkere en eerlijke voeding. Hun specifieke talenten en ervaringen zorgen voor de unieke mix die je ervaart wanneer je de shops of resto’s van Poule & Poulette bezoekt.
								Inne Vangeel komt uit een familie met diverse voedingspeciaalzaken en restaurants, Frederik
								Goossens behoort tot een familie waar de liefde voor interieurdesign en creativiteit in de genen zit en Filip Van Hoeck’s familie omvat generaties horeca-ondernemers.
								Samen willen ze Poule & Poulette uitbouwen tot een concept waar je de beste op houtvuur
								gebraden kippen van België koopt of waar je in een ongedwongen trendy resto-sfeer geniet van het beste wat Belgische kwaliteitskippen te bieden hebben.
								</p>
							</div>
                        </div>
                    </div>
				</div>
                
                <div id="display_1" class="contentsection normal">
					<div class="mcontent">
						<div class="inner clearfix">
							<div class="divider"></div>
                            <h1 class="text-center" style="margin-bottom: 30px;">Wie is wie?</h1>
							
							<div class="row">
								<div class="col-md-4 nav-box-container">
									<div class="nav-box team-box">
										<img src="{url}images/team/team-01.jpg" />
										<h3>Frederik Goossens</h3>
										<p class="text-center">Oprichter 2012</p>
										<p class="text-center" style="min-height: 60px;">Algemeen Management - Marketing – Look & Feel – Conceptbepaling – Franchise & Groeistrategie – Legal</p>
										<h4>Favoriete eten</h4>
										<p class="text-center" style="min-height: 40px;">Halve kip Poule & Poulette met extra veel appelmoes…</p>
									</div>
								</div>
								<div class="col-md-4 nav-box-container">
									<div class="nav-box team-box">
										<img src="{url}images/team/team-02.jpg" />
										<h3>Inne Vangeel</h3>
										<p class="text-center">Oprichter 2012</p>
										<p class="text-center" style="min-height: 60px;">Finanieel Management – Foodquality – Human Resource – Administratie</p>
										<h4>Favoriete eten</h4>
										<p class="text-center" style="min-height: 40px;">Caesar Salade met extra croutons. Mmmmmm</p>
									</div>
								</div>
								<div class="col-md-4 nav-box-container">
									<div class="nav-box team-box">
										<img src="{url}images/team/team-03.jpg" />
										<h3>Filip Van Hoeck</h3>
										<p class="text-center">Mede Oprichter 2014</p>
										<p class="text-center" style="min-height: 60px;">Operationeel Management – Technieken en constructie – Horeca – Exploitatie Controle</p>
										<h4>Favoriete eten</h4>
										<p class="text-center" style="min-height: 40px;">Videe met frietjes en Mayonaise!</p>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>

				<div id="display_2" class="contentsection normal">
					<div class="mcontent">
						<div class="inner clearfix">
                        
                            <div class="divider"></div>
                            <h1 class="text-center" style="margin-bottom: 80px;">Contact</h1>
							
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="job-info">
										<h4 class="text-center">Poule & Poulette Office</h4>
										<p class="text-center baconfont">
											Wijngaardbrug 8<br>
											2000 Antwerpen<br>
											T: +32 3 502 95 02
										</p>
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
							
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<?= Modules::run('contacts/contacts_logic/show_form', 'partial', 'contact'); ?>
								</div>
								<div class="col-md-2"></div>
							</div>
							
						</div>
					</div>
				</div>