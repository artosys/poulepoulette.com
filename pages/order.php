				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
				<div id="display_0" class="contentsection gray">
                    <div class="mcontent" style="max-width:  700px;">
						
						<?= Modules::run('contacts/contacts_logic/show_form', 'partial', 'order'); ?>
						
                    </div>
				</div>
                
                <div id="display_1" class="contentsection gray" style="padding-bottom: 0;">
					<div class="mcontent">
						<div class="inner clearfix">
							<div class="divider"></div>
                            <div id="imagegallery2">
								<div id="gallerystage" class="flexslider">
									<ul class="slides">
										<li><img src="{url}images/slider/menu-01.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-02.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-03.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-04.png" alt="galleryimage"></li>
										<li><img src="{url}images/slider/menu-05.png" alt="galleryimage"></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>