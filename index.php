<?php
// -----------------------------------------------------------------------------
//
// Artosys Client Website
//
// -----------------------------------------------------------------------------

/*
 * -----------------------------------------------------------------------------
 *  Define the environment variables
 * -----------------------------------------------------------------------------
 */
	define('ENVIRONMENT', 'offline');
	define('WEBSITE_KEYWORD', 'poule');
	define('DEV_STATUS', 'development'); // Development|testing|live
	
/*
 * -----------------------------------------------------------------------------
 *  Define connection with artosystem
 * -----------------------------------------------------------------------------
 */
	if(ENVIRONMENT == "offline")
	{
		error_reporting(E_ALL);
		include "path.php";
	}
	else
	{
		error_reporting(E_ALL);
		//define('SHAREPATH', "/lamp0/web/vhosts/www.artocdn.be/htdocs/php/");
		define('SHAREPATH', "../../php/");
	}

/*
 * -----------------------------------------------------------------------------
 *  Connect to artosystem
 * -----------------------------------------------------------------------------
 */
	include (SHAREPATH."artosystem.php");