$(function() {
	var sw = document.body.clientWidth, breakpoint = 900, mobile = true;
	var checkMobile = function() {
		mobile = (sw > breakpoint) ? false : true;
		movenavtoside();
	};
	jQuery(window).resize(function(){
		sw = document.body.clientWidth;	
		checkMobile();
	});

	function movenavtoside() {
		if (mobile) {
			jQuery('#placeholdernav').append(jQuery(".navelement"));
		} else {
			jQuery('#mainnav ul').append(jQuery(".navelement"));
		}
	}

	checkMobile();

	jQuery("#nav-open").click(function(){
		if (jQuery("body.opennavpanel").length > 0) {
			jQuery("body").removeClass("opennavpanel");
			jQuery("#maincontent, #footer, #header").animate({ left: -50 }, 500, function(){
				jQuery("#maincontent, #footer, #header").animate({ left: 0 }, 400);
			});
			jQuery("#navpanel").fadeOut();
		} else {
			jQuery("body").addClass("opennavpanel");
			jQuery("#maincontent, #footer, #header").animate({ left: '250px' }, 300);
			jQuery("#navpanel").css("width", "250px");
			jQuery("#navpanel").fadeIn();
		}
	});
	
	var i = 0;
       // get the total number of display sections
       //    where the ID starts with "display_"
    var len = $('div[id^="display_"]').length;


       // for next, increment the index, or reset to 0, and concatenate 
       //    it to "display_" and set it as the hash
    $('.nav-next a').click(function() {
        i = ++i % len;
        window.location.hash = "display_" + i;
        return false;
    });


       // for prev, if the index is 0, set it to the total length, then decrement
       //    it, concatenate it to "display_" and set it as the hash
    $('.nav-previous a').click(function() {
        if (!i) i = len;
        --i;
        window.location.hash = "display_" + i;
        return false;
    });
    
    if ( jQuery('#idinput_tijdstip').length > 0 )
    {
        jQuery('#idinput_tijdstip').datetimepicker({
          format:'d.m.Y H:i',
          inline:true,
          lang:'nl',
          theme: 'dark',
		  minTime: '10:00',
		  maxTime: '23:00'
        });    
    }
	
	jQuery("a.less").click(function(){
		var input_el = $(this).parent().children('input.input_aantal');
		var current_val = parseInt(input_el.val());
		var new_val = 0;
		
		if ( current_val == "" || isNaN(current_val) ) { current_val = 0; }
		
		if ( current_val > 0 )
		{
			new_val = Math.floor(current_val - 1);
		}
		else
		{
			new_val = 0;
		}
		
		input_el.val(new_val);
		
		return false;
	});
	
	jQuery("a.more").click(function(){
		var input_el = $(this).parent().children('input.input_aantal');
		var current_val = parseInt(input_el.val());
		var new_val = 0;
		
		if ( current_val == "" || isNaN(current_val) || current_val < 0 ) { current_val = 0; }
		
		if ( current_val < 5 )
		{
			new_val = Math.floor(current_val + 1);
		}
		else
		{
			new_val = 5;
		}
		
		input_el.val(new_val);
		
		return false;
	});
    
    jQuery("#expand").click(function(){
		if ( jQuery(this).parents('.widget-area-controls').hasClass('expanded') )
        {
            jQuery(this).parents('.widget-area-controls').removeClass('expanded');
            jQuery('#secondary').removeClass('expanded');  
        }
        else
        {
            jQuery(this).parents('.widget-area-controls').addClass('expanded');
            jQuery('#secondary').addClass('expanded');  
        }
	});

	jQuery(window).on("scroll", function(event){
		var windowScroll = jQuery(window).scrollTop();
		var windowHeight = jQuery(window).height();

		if (!mobile) {
			if (windowScroll > 40) {
				//jQuery("body").addClass("headermounted");
			} else {
				//jQuery("body").removeClass("headermounted");
			}
		}


		var distance = jQuery(window).scrollTop();
		var amount = 1 - (distance / 300);

		if (distance < jQuery("#feature").height()) {
			if (amount > .2) {
				jQuery("#feature img.slideimage" ).css('opacity', amount);
			}
			event.stopPropagation();
			event.preventDefault();
		}

		if (distance -300 > jQuery("#feature").height()) {
			jQuery("#uparrow").fadeIn();
			event.stopPropagation();
		} else {
			jQuery("#uparrow").fadeOut();
			event.stopPropagation();
		}

		// Custom Scrollspy
		if (jQuery("#mainnav li a.snwchanchor").length > 0) {
			mount();
		}

		// Bread anchor
		if ($("#bread").length > 0) {
			breadmount();
		}

		// Tag mount
		if ($("#tag").length > 0) {
			tagmount();
		}

		// Job mount
		if (jQuery(".jobtable").length > 0) {
			jobmount();
		}
	});
    
  // initialize the slideshow
  jQuery('.image img').fullscreenslides();
  
  // All events are bound to this container element
  var container = jQuery('#fullscreenSlideshowContainer');
  
  container
    //This is triggered once:
    .bind("init", function() { 

      // The slideshow does not provide its own UI, so add your own
      // check the fullscreenstyle.css for corresponding styles
      container
        .append('<div class="ui" id="fs-close">&times;</div>')
        .append('<div class="ui" id="fs-loader">Loading...</div>')
        .append('<div class="ui" id="fs-prev">&lt;</div>')
        .append('<div class="ui" id="fs-next">&gt;</div>')
      
      // Bind to the ui elements and trigger slideshow events
      jQuery('#fs-prev').click(function(){
        // You can trigger the transition to the previous slide
        container.trigger("prevSlide");
      });
      jQuery('#fs-next').click(function(){
        // You can trigger the transition to the next slide
        container.trigger("nextSlide");
      });
      jQuery('#fs-close').click(function(){
        // You can close the slide show like this:
        container.trigger("close");
      });
      
    })
    // When a slide starts to load this is called
    .bind("startLoading", function() { 
      // show spinner
      jQuery('#fs-loader').show();
    })
    // When a slide stops to load this is called:
    .bind("stopLoading", function() { 
      // hide spinner
      jQuery('#fs-loader').hide();
    })
    // When a slide is shown this is called.
    // The "loading" events are triggered only once per slide.
    // The "start" and "end" events are called every time.
    // Notice the "slide" argument:
    .bind("startOfSlide", function(event, slide) { 
      // set and show caption
      jQuery('#fs-caption span').text(slide.title);
      jQuery('#fs-caption').show();
    })
    // before a slide is hidden this is called:
    .bind("endOfSlide", function(event, slide) { 
      jQuery('#fs-caption').hide();
    });
  

	function mount() {
		var a,b,c;
		jQuery("li.navelement a.snwchanchor").each(function(){
			a = jQuery(jQuery(this).attr("href")).offset().top - 150;
			b = jQuery(jQuery(this).attr("href")).offset().top + jQuery(jQuery(this).attr("href")).next(".contentsection").height();
			c = jQuery(window).scrollTop();

			if (c >= a && c <= b) {
				jQuery("li.navelement a").removeClass("active");
				jQuery(this).addClass("active");
			} else {
				jQuery(this).removeClass("active");
			}
		});
	}


	var ypos = [65, 615, 1146, 1675, 2167, 2631, 3115, 3591];

	function breadmount() {
		var x = $($("#bread").parent().parent()).offset().top + $($("#bread").parent().parent()).outerHeight() -900;
		var z = $(window).scrollTop();
		var acdifference = x-z;
		if (x <= z) {
			//console.log(acdifference);
			if (acdifference < 0 && acdifference > -69) {
				$("#bread").css({backgroundPosition: "-"+ypos[0]+"px 0px"});
			} else if (acdifference <= -69 && acdifference >= -126) {
				$("#bread").css({backgroundPosition: "-"+ypos[1]+"px 0px"});
			} else if (acdifference <= -126 && acdifference >= -174) {
				$("#bread").css({backgroundPosition: "-"+ypos[2]+"px 0px"});
			} else if (acdifference <= -174 && acdifference >= -229) {
				$("#bread").css({backgroundPosition: "-"+ypos[3]+"px 0px"});
			} else if (acdifference <= -229 && acdifference >= -290) {
				$("#bread").css({backgroundPosition: "-"+ypos[4]+"px 0px"});
			} else if (acdifference <= -290 && acdifference >= -330) {
				$("#bread").css({backgroundPosition: "-"+ypos[5]+"px 0px"});
			} else if (acdifference <= -330 && acdifference >= -370) {
				$("#bread").css({backgroundPosition: "-"+ypos[6]+"px 0px"});
			} else if (acdifference <= -370 && acdifference >= -410) {
				$("#bread").css({backgroundPosition: "-"+ypos[7]+"px 0px"});
			} else if (acdifference > -410) {
				$("#bread").css({backgroundPosition: "-"+ypos[0]+"px 0px"});
			}
		} else {
			$("#bread").css({backgroundPosition: "-"+ypos[0]+"px 0px"});
		}
	}

	function tagmount() {
		// Distance from top
		var x = jQuery("#menunav").offset().top +200;
		// Height of tag
		var z = jQuery("#menunav").height();
		// Distance scrolled from top
		y = jQuery(window).scrollTop();
		acdifference = Math.round(x-z);

		// Increment starting from 1 when section is visible
		current = Math.round((y - x));

		if (y <= x +2 && y >= acdifference) {
			jQuery("#tag").css("top", current);
		} else {
			jQuery("#tag").css("top", "0");
		}
	}

	function jobmount() {
		// Distance from top
		var x = jQuery(".jobtable").parent().offset().top;
		// Height of tag
		var z = jQuery(".jobtable").height();
		// Distance scrolled from top
		y = jQuery(window).scrollTop();
		acdifference = Math.round(x-z);

		// Increment starting from 1 when section is visible
		current = Math.round((y - x));

		if (y <= x +2 && y >= acdifference -100) {
			jQuery(".jobtable").each(function(){
				jQuery(this).addClass("goto"+jQuery(this).data("menu"));
			});
		} else {
			jQuery(".jobtable").each(function(){
				jQuery(this).removeClass("goto"+jQuery(this).data("menu"));
			});
		}
	}

	jQuery('a.snwchanchor, #mainnav .logoanchor').click(function(){
		jQuery('html, body').stop().animate({
			scrollTop: jQuery( jQuery(this).attr('href') ).offset().top - 80
		}, 1000, "easeInOutCirc");
		return false;
	});

	jQuery("#chooselocation").click(function(event){
		event.preventDefault();
		jQuery(".locationlist").slideToggle();
	});

	var marker;
	var map;
	var mapzoom = "";

	mapzoom = 8;
    var MY_MAPTYPE_ID = 'custom_style';

	function initialize() {
		var featureOpts = [
        {
          stylers: [
            { "saturation": -100 },
             { "lightness": -8 },
             { "gamma": 1.18 }
          ]
        }
        ];
  
        var mapOptions = {
			zoom: jQuery(".sndwchmap").data("zoom"),
			scrollwheel: false,
			mapTypeControl: false,
			disableDoubleClickZoom: false,
			disableDefaultUI: false,
			draggable: true,
			center: new google.maps.LatLng(jQuery(".sndwchmap").data("lat"), jQuery(".sndwchmap").data("long")),
			mapTypeId: MY_MAPTYPE_ID
		};

		map = new google.maps.Map(document.getElementsByClassName('sndwchmap')[0], mapOptions); 
        
        var styledMapOptions = {
            name: 'Custom Style'
          };
          
          var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

		var i = 1;
		jQuery(".locationitem").each(function(){
			var pinIcon = new google.maps.MarkerImage(
			    "http://mt.google.com/vt/icon?psize=12&font=fonts/Roboto-Bold.ttf&color=fC141f11&name=icons/spotlight/spotlight-waypoint-b.png&ax=43&ay=50&text="+i+"&scale=2",
			    null, /* size is determined at runtime */
			    null, /* origin is 0,0 */
			    null, /* anchor is bottom center of the scaled image */
			    new google.maps.Size(22, 40)
			);

			i++;
            
            var contentString = '<div id="maps-up"><h4 style="margin-bottom: 5px;">'+
                  jQuery(this).children("h2").text()+'</h4>'+
				  '<div>'+ jQuery(this).find(".adres-gegevens").html() +'</div>'+
                  '</div>';
            
              var infowindow = new google.maps.InfoWindow({
                content: contentString
              });

			marker = new google.maps.Marker({
				map:map,
				draggable:false,
				icon:pinIcon,
				animation: google.maps.Animation.DROP,

				position: new google.maps.LatLng(jQuery(this).data("lat"), jQuery(this).data("long")),
			});
            marker.addListener('click', function() {
                infowindow.open(map, this);
              });
		});
	}

	if (jQuery(".sndwchmap").length > 0) {
		initialize();
	}

	jQuery("."+jQuery("#menunav li a.active").data("menu")).show();

	jQuery("#menunav a").not(".menuheader >").click(function(event){
		event.preventDefault();
		jQuery("#menunav .active").removeClass("active");
		jQuery(this).addClass("active");
		clockreset();
		jQuery(".menusection").hide();
		jQuery("."+jQuery(this).data("menu")).show();
		// Scroll to top of menu
		jQuery('html, body').stop().animate({
			scrollTop: jQuery("#menunav").offset().top - 160
		}, 1000, "easeInOutCirc");
		return false;
	});

	function clockreset() {
		jQuery(".gotobreakfast").removeClass("gotobreakfast");
		jQuery(".gotolunch").removeClass("gotolunch");
		jQuery(".gotodinner").removeClass("gotodinner");
	}

	// Feature slideshow loads immediately
	jQuery('#feature.flexslider').flexslider({
        animation: "fade",      
        directionNav: true,
        prevText: "&lsaquo;",
        nextText: "&rsaquo;",
		controlsContainer: ".controls",
        start: function(slider){
          jQuery('body').removeClass('loading');
        }
    });

    // Lifestyle slideshow loads at very end
	jQuery(window).load(function(){
      jQuery('#imagegallery .flexslider').flexslider({
        animation: "slide",        
        directionNav: true,
        prevText: "&lsaquo;",
        nextText: "&rsaquo;",
      });
    });
    
    jQuery(window).load(function(){
      jQuery('#imagegallery2 .flexslider').flexslider({
        animation: "slide",        
        directionNav: true,
        prevText: "&lsaquo;",
        nextText: "&rsaquo;",
      });
    });
	
	jQuery(window).load(function(){
      jQuery('#imagegallery3 .flexslider').flexslider({
        animation: "slide",        
        directionNav: true,
        prevText: "&lsaquo;",
        nextText: "&rsaquo;",
      });
    });

	// Set all events to the same height
	jQuery(".eventitem").each(function(){

	});

	var maxHeight = -1;
	var totalcols = jQuery(".eventcaption h2").length;

	function resetheights () {
		// Reset the heights to auto
		jQuery('.eventcaption h2').each(function() {
			jQuery(this).height("auto");
		});

		maxHeight = 0;
		// Look through all elements and track the heights based on their position in their stack and apply to others in similar positions.
		for (var i=0; i < totalcols + 1; i++) {
			// Set our height tracker to zero


			jQuery('.eventcaption h2:nth-child('+i+')').each(function() {
			     maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
			});

			jQuery('.eventcaption h2:nth-child('+i+')').each(function() {
				jQuery(this).height(maxHeight);
			});
		}
	}

	resetheights();

});