
$(document).ready(function(){
	// Cart popup
	$(document).on('click', '#header > .cart .info', function(){
		if($("#header > .cart").hasClass('open'))
		{
			$("#header > .cart").removeClass('open');
			$("#header > .cart .cart_popup").slideUp();
		}
		else
		{
			$("#header > .cart").addClass('open')
			$("#header > .cart .cart_popup").slideDown();
		}
	});
});

function handle_cart_add(cart)
{
	$("#header > .cart").html(cart);
	$("#header > .cart").addClass('open')
	$("#header > .cart .cart_popup").slideDown();
}