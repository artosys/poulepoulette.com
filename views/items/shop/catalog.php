<div class="row">
	<?php foreach($categories as $category){ ?>
	<div class="col-sm-12 col-md-6">
		<a href="{url}items/category/<?= $category->id ?>/<?= urlencode($category->name) ?>" class="shop_block">
			<?= $category->name ?>
		</a>
	</div>
	<?php } ?>
</div>