<?php if ($identifier == 'main_view') { $aantal_posts = count($posts); ?>
	
	<?php if ( count($posts) == 0 ) { 
		
		echo '<h4 class="text-center">Het eerste nieuwsbericht zal niet lang meer op zich laten wachten<br><br>Kom weldra nog eens kijken of volg ons alvast op Facebook</h4>'; 
		
		} else { ?>

		<div class="widget Blog" id="Blog1">
			<div class="blog-posts hfeed clearfix" id="grid">

			<?php foreach($posts as $post){ ?>

										<div class="animated">
                            				<div class="post hentry clearfix">
                            					<div class="post-body entry-content">
															<?php 
																setlocale(LC_TIME, 'nl_NL');	
																$datetime = new DateTime($post->date_created);
															?>
													<div class="news-date">
                                                        <?= $datetime->format('d') ?> <?= $datetime->format('F').' '.$datetime->format('Y') ?>
                       								</div>
                                                    <div>
                            							<div class="<?php if (count($post->videos) == 0) { ?>featured-image-container<?php } ?>">
															<?php if ( count($post->pictures) > 0 ) { ?>
                            								<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>">
                            								    <img alt="<?= $post->title ?>" src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" >
                            								</a>
															<?php } elseif ( count($post->videos) > 0 ) { if ( strpos($post->videos[0]->url,'v=') !== false ) { $videourl = "https://www.youtube.com/embed/".explode('v=',$post->videos[0]->url)[1]; } else { $videourl = $post->videos[0]->url; } ?>
											
															<iframe height="223" width="100%" src="<?= $videourl ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

															<?php } ?>
					                                    </div>	
                            							<div class="entry-wrap">
                            								<h3 class="entry-header">
                            									<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= $post->title ?></a>
                            								</h3>
		
                            								<div style="max-height: 80px; overflow:hidden; margin-bottom: 20px;"><?= nl2br(substr($post->text, 0, 330)) ?></div>
                            								
                                                            <a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>" class="read-more" style="margin: 10px 15px;"><i class="fa fa-eye"></i>Meer lezen</a>
                                                            <a href="{url}posts/like/<?= $post->id ?>" class="read-more" style="margin: 10px 15px;"><i class="fa fa-thumbs-up"></i>Vind ik leuk</a>
                                                            <a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments" class="read-more" style="margin: 10px 15px;"><i class="fa fa-comments"></i>Reageren</a>
                                                            <a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)" class="read-more" style="margin: 10px 15px;"><i class="fa fa-share"></i>Delen op Facebook</a>
                            							
                            							</div>
                            						</div>
                            						
                            						<div style="clear: both;"></div>
                            					</div>
                            				</div>
                            			</div>							
		
			<?php } ?>
		
			</div>
		</div>

	<!-- Pagination -->
	<?php if($paginate && 1 == 2){ ?>
	<?php if ( isset($_GET['cat']) ) { $paging_url = $paging_url.'?cat='.$_GET['cat']; } else { $paging_url = $paging_url.'?news=go'; } ?>
	<div class="pagination standard_left pagination_with_border" style="border-top-color:#dedede; border-top-width:1px; border-top-style:solid; padding-top:50px;text-align:center; ">
                                                <ul>
                                                    <li class="prev">
														<?php if($page > 0){ ?>
														<a href="<?= $paging_url."&page=".($page - 1) ?>">
                                                            <span class="pagination_arrow arrow_carrot-left"></span>
                                                        </a>
														<?php } else { ?>
														<a href="#">
														<span class="pagination_arrow arrow_carrot-left"></span>
														</a>
														<?php } ?>
                                                    </li>
													<?php 
													for($i = 0; $i < ceil($all_posts / $settings->page_count); $i++)
													{  
														if($i == $page)
														{
															?>
															<li class="active">
																<span><?= $i+1 ?></span>
															</li>
															<?php
														}
														else
														{
															?>
															<li>
																<a href="<?= $paging_url."&page=".$i ?>" class="inactive"><?= $i+1 ?></a>
															</li>
															<?php
														}
													} 
													?>
                                                    <li class="next">
														<?php if($page < ceil($all_posts / $settings->page_count)-1){ ?>
														<a href="<?= $paging_url."&page=".($page + 1) ?>">
                                                            <span class="pagination_arrow arrow_carrot-right"></span>
                                                        </a>
														<?php } else { ?>
														<a href="#">
														<span class="pagination_arrow arrow_carrot-right"></span>
														</a>
														<?php } ?>
                                                    </li>
                                                </ul>
	</div>
	<?php } ?>


	<?php } ?>

<?php } elseif ($identifier == 'sidebar_view') { ?>
	
	<?php if ( count($posts) == 0 ) { 
		
		echo '<li>Er zijn nog geen nieuwsartikels.</li>'; 
		
		} else { ?>

	<?php foreach($posts as $post){ ?>

										<li>
                                    		<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= $post->title ?></a>
                                   		</li>
	
	<?php } ?>

	<?php } ?>
	
<?php } elseif ($identifier == 'homepage_view') { ?>
	
	<?php if ( count($posts) == 0 ) { 
		
		echo '<h4 class="text-center">zzzHet eerste nieuwsbericht zal niet lang meer op zich laten wachten<br><br>Kom weldra nog eens kijken of volg ons alvast op Facebook</h4>'; 
		
		} else { ?>

	<?php foreach($posts as $post){ ?>
										
								<div class="col-md-4 nav-box-container">
    								<div class="nav-box blog-home">
                                        <?php if ( count($post->pictures) > 0 ) { ?>
                                        <img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" class="blog-image" style="max-height: 194px;" />
                                        <?php } elseif ( count($post->videos) > 0 ) { if ( strpos($post->videos[0]->url,'v=') !== false ) { $videourl = "https://www.youtube.com/embed/".explode('v=',$post->videos[0]->url)[1]; } else { $videourl = $post->videos[0]->url; } ?>
											
											<iframe height="194" width="100%" src="<?= $videourl ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

										<?php } ?>
    									<h3><?= $post->title ?></h3>
                                        
										<?php 
											setlocale(LC_TIME, 'nl_NL');	
											$datetime = new DateTime($post->date_created);
										?>
                                        <div class="blog-date"><?= $datetime->format('d') ?> <?= $datetime->format('F').', '.$datetime->format('Y') ?></div>
    									
    									<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>" class="read-more" style="margin: 10px 15px;"><i class="fa fa-eye"></i>Meer lezen</a>
    								</div>
                                </div>
	
	<?php } ?>

	<?php } ?>
	
<?php }  ?>
	
	

<script type="text/javascript">
	function fbShare(url, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }
</script>