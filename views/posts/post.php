				<div class="top-area-default">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                </div>
                
				<div class="contentsection gray" style="border-top: none;">
                    <div class="mcontent">
						<div class="inner clearfix">
                        
                            <div class="row nieuwsartikel">
                                
                                        <div class="animated">
                            				<div class="post hentry clearfix">
                            					<div class="post-body entry-content">
															<?php 
																setlocale(LC_TIME, 'nl_NL');	
																$datetime = new DateTime($post->date_created);
															?>
													<div class="news-date">
                                                        <?= $datetime->format('d') ?> <?= $datetime->format('F').' '.$datetime->format('Y') ?>
                       								</div>
                                                    <div>
                            							<div class="<?php if (count($post->videos) == 0) { ?>featured-image-container<?php } ?>">
															<?php if ( count($pictures) > 1 ) { ?>
															<div id="imagegallery3">
																<div id="gallerystage" class="flexslider">
																	<ul class="slides">
																		<?php foreach($pictures as $picture){ ?>
																		<li><img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->src_original ?>" alt="galleryimage"></li>
																		<?php } ?>
																	</ul>
																</div>
															</div>
															<?php } elseif ( count($pictures) > 0 ) { ?>
                            								<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>">
                            								    <img alt="<?= $post->title ?>" src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$pictures[0]->src_original ?>" >
                            								</a>
															<?php } elseif ( count($post->videos) > 0 ) { if ( strpos($post->videos[0]->url,'v=') !== false ) { $videourl = "https://www.youtube.com/embed/".explode('v=',$post->videos[0]->url)[1]; } else { $videourl = $post->videos[0]->url; } ?>
											
															<iframe height="689" width="100%" src="<?= $videourl ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

															<?php } ?>
					                                    </div>	
                            							<div class="entry-wrap">
                            								<h3 class="entry-header">
                            									<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= $post->title ?></a>
                            								</h3>
															
															<div class="social-stats">
																<a href="#" class="read-more"><i class="fa fa-thumbs-up"></i><?= count($likes) ?></a>
																<a href="#" class="read-more"><i class="fa fa-comments"></i><?= count($comments) ?></a>
																<a href="#" class="read-more"><i class="fa fa-share"></i><?= $post->shared ?></a>
															</div>
		
                            								<div><?= nl2br($post->text) ?></div>
                            								
                                                            <a href="{url}posts/like/<?= $post->id ?>" class="read-more" style="margin: 10px 15px;"><i class="fa fa-thumbs-up"></i>Vind ik leuk</a>
                                                            <a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments" class="read-more" style="margin: 10px 15px;"><i class="fa fa-comments"></i>Reageren</a>
                                                            <a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)" class="read-more" style="margin: 10px 15px;"><i class="fa fa-share"></i>Delen op Facebook</a>
                            							
                            							</div>
                            						</div>
                            						
                            						<div style="clear: both;"></div>
                            					</div>
                            				</div>
                            			</div>	
                               
							</div>
							
							<?php if($post->allow_comments == 1){ ?>
							<?php if(count($comments) > 0){ ?>
								<div class="divider"></div>

								<h3><?= count($comments) ?> Reactie<?= count($comments) == 1 ? "":"s" ?></h3>
								
								<?php foreach($comments as $comment){ ?>
								<div class="comment">

									<div class="comment-name">
										<?= $comment->name ?>
										<div class="comment-date"><?= strftime("%d %B %Y", strtotime($comment->date_created)) ?></div>
									</div>

									<div class="comment-message"><?= nl2br($comment->comment) ?></div>

								</div>
								<?php } ?>

								<div class="divider-empty"></div>
							<?php } ?>
							
							<div class="divider"></div>
							
							<h3 style="margin-bottom: 30px;">Plaats een reactie</h3>
							
							<form action="{url}posts/comment/<?= $post->id ?>" class="" id="post_comment"  method="post">
								<div class="row" id="post_comments">	
									<div class="col-sm-12">
										<div class="form-input">
											<input type="text" name="name" id="comment_name" placeholder="Uw naam" value="" />
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-input">
											<input type="email" name="email" id="comment_email" placeholder="Uw e-mail">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-input">
											<textarea name="comment" id="comment_comment" rows = "5" placeholder="Uw reactie"></textarea>
										</div>
									</div>
									<div class="col-md-12 form_row actions">
										<input type="submit" name="btn_comment" value="Reactie toevoegen">
									</div>
								</div>
								
							</form>
							<?php } ?>
							
                    </div>
				</div>
              </div>


		<script type="text/javascript">
			$(document).ready(function(){
				$("#post_comment").submit(function(e) {
					comment_valid = true;
					$("#comment_error").text('');
					if($.trim($("#comment_name").val()) == "")
					{
						comment_valid = false;
					}
					var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					if($.trim($("#comment_email").val()) == "" || !re.test($("#comment_email").val()))
					{
						comment_valid = false;
					}
					if($.trim($("#comment_comment").val()) == "")
					{
						comment_valid = false;
					}
					if(comment_valid == false)
					{
						e.preventDefault();
						$("#comment_error").text('Gelieve alle velden correct en volledig in te vullen.');
						$("#comment_error").show();
					}
				});
			});
			function fbShare(url, winWidth, winHeight) {
				var winTop = (screen.height / 2) - (winHeight / 2);
				var winLeft = (screen.width / 2) - (winWidth / 2);
				window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
			}
		</script>