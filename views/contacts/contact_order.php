	<?= $success_message != "" ? "<div class='success'>".$success_message."</div>" : ""; ?>
	<?= $error_message != "" ? "<div class='error'>".$error_message."</div>" : ""; ?>
	<form action="" class="" id="newsletterform"  role="form" name="form_contact" method="post" enctype="multipart/form-data" style="margin-top: 0;">
	<div class="inner clearfix">
	<div class="divider-empty"></div>
	<h1 class="text-center" style="margin-bottom: 40px;">Bestel online</h1>
	
	<div class="job-info baconfont">
		<p>Geen tijd verliezen of zeker zijn van uw kip ?</p>
		<p>Laat uw bestelling dan klaarzetten via onze website</p>
		<p>Plaats via onderstaand formulier minstens 2 uur op voorhand uw bestelling en wij zorgen dat alles voor u klaarstaat</p>
	</div>
    
	<div class="row">
		<h3 class="text-center" style="margin: 20px;">1. Uw gegevens</h3>
		<div class="order-box">
		<?php foreach($fields as $field){ ?>
			
				<?php 
				switch($field->label)
				{
					case "Aanspreking":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<select
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>"
								>
									<option value="Dhr." <?= $field->value == 'Dhr.' ? 'selected="selected"':'' ?>>Dhr.</option>
									<option value="Mevr." <?= ($field->value == 'Mevr.' || empty($field->value)) ? 'selected="selected"':'' ?>>Mevr.</option>
								</select>
							</div>
						</div>
						<?php
						break;
					case "Achternaam":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "E-mail":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "Telefoonnummer":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "Geboortedatum":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?> (dd/mm)"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
				}
				?>
		<?php } ?>
		</div>
	</div>
	<div class="row">
		<h1 class="text-center" style="margin: 40px;">+</h1>
                            
        <h3 class="text-center" style="margin: 20px;">2. Kies een Poule &amp; Poulette shop</h3>
		<div class="order-box">
		<div class="col-md-3"></div>
		<?php foreach($fields as $field){ ?>
			
				<?php 
				switch($field->label)
				{
					case "Shop":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<select
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>"
								>
									<option value="Brasschaat" <?= ($field->value == 'Brasschaat' || empty($field->value)) ? 'selected="selected"':'' ?>>Bredabaan 271, 2930 Brasschaat</option>
									<option value="Wijngaardbrug" <?= $field->value == 'Wijngaardbrug' ? 'selected="selected"':'' ?>>Wijngaardbrug 8, 2000 Antwerpen</option>
								</select>
							</div>
						</div>
						<?php
						break;
				}
				?>
		<?php } ?>
		<div class="col-md-3"></div>
		<div class="col-md-12 text-center" style="padding: 10px 0;">
			Mocht uw gemeente hier niet tussen staan, gelieve dan telefonisch te bestellen
		</div>
		</div>
	</div>
	<div class="row">
		<h1 class="text-center" style="margin: 40px;">+</h1>
                            
        <h3 class="text-center" style="margin: 20px;">3. Tijdstip van afhaling</h3>
		<div class="order-box">
		<div class="col-md-3"></div>
		<?php foreach($fields as $field){ ?>
			
				<?php 
				switch($field->label)
				{
					case "Tijdstip":
						?>
						<div class="col-sm-6">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
				}
				?>
		<?php } ?>
		<div class="col-md-3"></div>
		<div class="col-md-12 text-center" style="padding: 10px 0;">
			Indien u uw bestelling binnen de 2 uur wilt afhalen, gelieve dan telefonisch te bestellen
		</div>
		<div class="col-md-12 text-center" style="padding: 10px 0;">
			Gelieve op feestdagen enkel telefonisch te bestellen
		</div>
		</div>
	</div>
	<div class="row">
		<h1 class="text-center" style="margin: 40px;">+</h1>
                            
        <h3 class="text-center" style="margin: 20px;">4. Kies uw product(en)</h3>
		<div class="order-box">
		<?php foreach($fields as $field){ ?>
			
				<?php 
				switch($field->label)
				{
					case "Halve kip Classic":
						?>
						<div class="row">
								<div class="col-md-6 input_label">
									<?= $field->label ?> 
									<p>Een kwaliteitskip, gebraden op houtvuur.<br />Gekruid met klassieke kipkruiden.</p>
                                </div>
                                <div class="col-md-6 text-center">
									<a href="" class="less" >-</a>
                                    <input 
										type="text" 
										name="<?= $this->contact->form_name($field->label) ?>" 
										id="<?= $this->contact->form_id($field->label) ?>" 
										class="input_aantal" 
										placeholder="0" 
										maxlength="1" 
										value="<?= $field->value ?>"
									/>
                                    <a href="" class="more" >+</a>
                                </div>
						</div>
						<?php
						break;
					case "Halve kip Poule & Poulette":
						?>
						<div class="row">
								<div class="col-md-6 input_label">
									<?= $field->label ?> 
									<p>Een kwaliteitskip, gebraden op houtvuur.<br />Gemarineerd met verse tijm, look &amp; citroen.</p>
                                </div>
                                <div class="col-md-6 text-center">
									<a href="" class="less" >-</a>
                                    <input 
										type="text" 
										name="<?= $this->contact->form_name($field->label) ?>" 
										id="<?= $this->contact->form_id($field->label) ?>" 
										class="input_aantal" 
										placeholder="0" 
										maxlength="1" 
										value="<?= $field->value ?>"
									/>
                                    <a href="" class="more" >+</a>
                                </div>
						</div>
						<?php
						break;
					case "Hele kip Classic":
						?>
						<div class="row">
								<div class="col-md-6 input_label">
									<?= $field->label ?> 
									<p>Een hele kip versneden om met het gezin van te genieten.</p>
                                </div>
                                <div class="col-md-6 text-center">
									<a href="" class="less" >-</a>
                                    <input 
										type="text" 
										name="<?= $this->contact->form_name($field->label) ?>" 
										id="<?= $this->contact->form_id($field->label) ?>" 
										class="input_aantal" 
										placeholder="0" 
										maxlength="1" 
										value="<?= $field->value ?>"
									/>
                                    <a href="" class="more" >+</a>
                                </div>
						</div>
						<?php
						break;
					case "Hele kip Poule & Poulette":
						?>
						<div class="row">
								<div class="col-md-6 input_label">
									<?= $field->label ?> 
                                </div>
                                <div class="col-md-6 text-center">
									<a href="" class="less" >-</a>
                                    <input 
										type="text" 
										name="<?= $this->contact->form_name($field->label) ?>" 
										id="<?= $this->contact->form_id($field->label) ?>" 
										class="input_aantal" 
										placeholder="0" 
										maxlength="1" 
										value="<?= $field->value ?>"
									/>
                                    <a href="" class="more" >+</a>
                                </div>
						</div>
						<?php
						break;
					case "Kip versneden":
						?>
						<div style="height: 0; border-top: 1px solid #717171; margin: 10px 0;"></div>
						<div class="row">
								<div class="col-md-6 input_label">
									<?= $field->label ?> 
                                </div>
                                <div class="col-md-6 text-center">
                                    <input 
										type="checkbox" 
										name="<?= $this->contact->form_name($field->label) ?>" 
										id="<?= $this->contact->form_id($field->label) ?>" 
										<?= $field->value == true ? 'checked="checked"':'' ?> 
										style="margin-top: 15px;" />
                                </div>
						</div>
						<?php
						break;
					case "Jus op de kip":
						?>
						<div class="row">
								<div class="col-md-6 input_label">
									<?= $field->label ?> 
                                </div>
                                <div class="col-md-6 text-center">
                                    <input 
										type="checkbox" 
										name="<?= $this->contact->form_name($field->label) ?>" 
										id="<?= $this->contact->form_id($field->label) ?>" 
										<?= $field->value == true ? 'checked="checked"':'' ?> 
										style="margin-top: 15px;" />
                                </div>
						</div>
						<?php
						break;
				}
				?>
		<?php } ?>
			<div class="col-md-12 text-center" style="padding: 10px 0;">
				Bij afhaling kan u nog uw bijhorende bijgerechten kiezen zoals<br />
				Salades, sauzen, appelmoes, soepen, etc...
			</div>
		</div>
	</div>
	<div class="row">
		<h1 class="text-center" style="margin: 40px;">+</h1>
                            
        <h3 class="text-center" style="margin: 20px;">5. Bijkomende opmerkingen</h3>
		<div class="order-box">
		<?php foreach($fields as $field){ ?>
			
				<?php 
				switch($field->label)
				{
					case "Opmerkingen":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<textarea 
									name="<?= $this->contact->form_name($field->label) ?>" 
									rows = "5"  
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
								><?= $field->value ?></textarea>
							</div>
						</div>
						<?php
						break;
				}
				?>
		<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 form_row actions">
			<input type="submit" id="submit" class="" name="btn_submit" value="Bestelling plaatsen" />
		</div>
	</div>
	</div>
	</form>