	<?= $success_message != "" ? "<div class='success'>".$success_message."</div>" : ""; ?>
	<?= $error_message != "" ? "<div class='error'>".$error_message."</div>" : ""; ?>
	<form action="" class="" id="newsletterform"  role="form" name="form_contact" method="post" enctype="multipart/form-data" style="margin-top: 0;">
	<h3 class="text-center">Stuur ons een bericht</h3>
	<div class="row">	
		<?php foreach($fields as $field){ ?>
			
				<?php 
				switch($field->label)
				{
					case "Naam":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "Bedrijf":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "E-mailadres":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "Telefoonnummer":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<input 
									type="text" 
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
									value="<?= $field->value ?>"
								/>
							</div>
						</div>
						<?php
						break;
					case "Afdeling":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<select
									name="<?= $this->contact->form_name($field->label) ?>" 
									id="<?= $this->contact->form_id($field->label) ?>"
								>
									<option value="Finance" <?= $field->value == 'Finance' ? 'selected="selected"':'' ?>>Finance</option>
									<option value="Marketing" <?= $field->value == 'Marketing' ? 'selected="selected"':'' ?>>Marketing</option>
									<option value="Aankoop" <?= $field->value == 'Aankoop' ? 'selected="selected"':'' ?>>Aankoop</option>
									<option value="Legal" <?= $field->value == 'Legal' ? 'selected="selected"':'' ?>>Legal</option>
									<option value="Jobs" <?= $field->value == 'Jobs' ? 'selected="selected"':'' ?>>Jobs</option>
									<option value="Franchise" <?= $field->value == 'Franchise' ? 'selected="selected"':'' ?>>Franchise</option>
									<option value="Admin" <?= ($field->value == 'Admin' || empty($field->value)) ? 'selected="selected"':'' ?>>Algemene vragen</option>
								</select>
							</div>
						</div>
						<?php
						break;
					case "Bericht":
						?>
						<div class="col-sm-12">
							<div class="form-input">
								<textarea 
									name="<?= $this->contact->form_name($field->label) ?>" 
									rows = "5"  
									id="<?= $this->contact->form_id($field->label) ?>" 
									placeholder="<?= $field->label ?>"
								><?= $field->value ?></textarea>
							</div>
						</div>
						<?php
						break;
				}
				?>
		<?php } ?>
		<div class="col-md-12 form_row actions">
			<input type="submit" id="submit" class="" name="btn_submit" value="Bericht verzenden" />
		</div>
		</div>
	</form>