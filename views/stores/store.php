				<div class="top-area-default" style="height: 245px;">
                    <img src="{url}images/logo-long.png?l=<?= time() ?>" id="logo-long" />
					<img src="{url}images/logo-long-mobile.png?l=<?= time() ?>" id="logo-long-mobile" />
                    <h1 class="text-center" style="padding: 190px 0 20px; font-size:48px; color: #fff;">
                        <?= $store->name ?>
                    </h1>
                </div>
				
				<?php if(count($pictures) > 0){ ?>
               <div class="prefooter">
					<div id="stamps" class="image">
						<?php foreach($pictures as $picture){ ?>
						<a rel="gallery" title="Title 1" href="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->src_original ?>">
						<img class="landscape" src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->src_original ?>" width="474" height="316" alt="stampimage">
						</a>
						<?php } ?>
					</div>
				</div> 
				<?php } ?>
                
				<div class="contentsection gray" style="padding-bottom: 0;">
                    <div class="mcontent">
						<div class="inner clearfix">
                        <div class="divider-empty"></div>
							
							<div class="row">
                                <div class="col-md-12">
										<div class="nav-box" style="margin-bottom: 20px;">
                                            <div class="circle-icon" id="icn-info"></div>
                                            <?= nl2br($store->text) ?>
        								</div>
								</div>
							</div>
						
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="nav-box">
                                            <div class="circle-icon" id="icn-phone"></div>
                                            <h5 style="margin-bottom: 20px;">
                                                <a href='mailto:<?= $store->email ?>'><?= $store->email ?></a>
                                            </h5>
											<p style="font-size: 11px;">( Geen bestellingen per e-mail aub )</p>
											<a href="tel:<?= $store->phone ?>">
											<h3 style="margin-bottom: 20px;"><?= $store->phone ?></h3>
											</a>
											<p style="font-size: 11px;">( klik om rechtstreeks met uw gsm te bellen )</p>
        								</div>
                                        
                                        <div class="nav-box" style="margin-top: 80px; margin-bottom: 80px;">
                                            <div class="circle-icon" id="icn-clock"></div>
        									<ul class="lijst" style="margin-top: 0;">  
												<?php 
												$names = $this->config->item('cal_days');
												for($i = 0; $i < 8; $i++)
												{
													echo "<li>";
													echo $names[$i];

													if(isset($openhours[$i+1]))
													{
														$start1 = strtotime($openhours[$i+1]->start_1);
														$start2 = strtotime($openhours[$i+1]->start_2);
														$end1   = strtotime($openhours[$i+1]->end_1);
														$end2   = strtotime($openhours[$i+1]->end_2);

														if($start2 == $end1)
														{
															$extra = "";
															if(date('H',$end1) > 12)
																$extra = " style='text-align: right;'";
															echo "<span>".date("H\ui",$start1)." - ".date("H\ui",$end2)."</span>";
														}
														else
														{
															echo "<span>".date("H\ui",$start1)." - ".date("H\ui",$end1);
															echo " en ".date("H\ui",$start2)." - ".date("H\ui",$end2)."</span>";
														}
													}
													else
													{
														echo "<span>Gesloten</span>";
													}
													echo "</li>";
												}
												?>
                                            </ul>
        								</div>
										
										<div class="nav-box" style="margin-top: 80px; margin-bottom: 80px;">
                                            <div class="circle-icon" id="icn-jobs"></div>
        									<?= Modules::run('teams/teams_logic/index', 'partial', 0, $store->id ); ?>
        								</div>
										
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="nav-box locationitem" 
										 <?php if ( $store->id == 4 ) { ?>
											data-lat="51.2880117" data-long="4.4872306"
											<?php } ?>
											<?php if ( $store->id == 5 ) { ?>
											data-lat="51.2184281" data-long="4.3968499"
											<?php } ?>
											<?php if ( $store->id == 6 ) { ?>
											data-lat="51.22128" data-long="4.4051375"
											<?php } ?>
										 >
                                        <div class="circle-icon" id="icn-locaties"></div>
       									<h3 class="adres-gegevens"><?= nl2br($store->address) ?></h3>
										<p>
											<?php if ( $store->id == 4 ) { ?>
											@ Gemeentehuis
											<?php } ?>
											<?php if ( $store->id == 5 ) { ?>
											@ voetgangerstunnel
											<?php } ?>
											<?php if ( $store->id == 6 ) { ?>
											@ Carolus Borromeuskerk
											<?php } ?>
										</p>
                                            <div class="tags clearfix" style="margin-bottom: 30px;">
        									   <?php if ( $store->id == 4 ) { ?>
												<span>Shop</span>
												<?php } ?>
												<?php if ( $store->id == 5 ) { ?>
												<span>Resto</span>
												<?php } ?>
												<?php if ( $store->id == 6 ) { ?>
												<span>Shop</span> + <span>Eetkamer</span>
												<?php } ?>
        									</div>
                                    </div>
                                    
                                    <div class="clearfix">
        							
                                    <div class="boxedmap bordermap" style="margin-top: 0;">
                				        <div class="sndwchmap" data-zoom="13" 
											  <?php if ( $store->id == 4 ) { ?>
											data-lat="51.2880117" data-long="4.4872306"
											<?php } ?>
											<?php if ( $store->id == 5 ) { ?>
											data-lat="51.2184281" data-long="4.3968499"
											<?php } ?>
											<?php if ( $store->id == 6 ) { ?>
											data-lat="51.22128" data-long="4.4051375"
											<?php } ?>
											 ></div>
                				    </div>
										
									<div class="nav-box" style="margin-top: 80px; margin-bottom: 80px;">
                                            <div class="circle-icon" id="icn-facebook"></div>
        									<div class="apply">
												<div class="wbutton">
													 <?php if ( $store->id == 4 ) { ?>
													<a href="https://www.facebook.com/poulepoulette.brasschaat.bredabaan" target="_blank">Volg ons op Facebook</a>
													<?php } ?>
													<?php if ( $store->id == 5 ) { ?>
													<a href="https://www.facebook.com/poulepoulette.antwerpen.sintjansvliet" target="_blank">Volg ons op Facebook</a>
													<?php } ?>
													<?php if ( $store->id == 6 ) { ?>
													<a href="https://www.facebook.com/poulepoulette.antwerpen.wijngaardbrug" target="_blank">Volg ons op Facebook</a>
													<?php } ?>
												</div>
											</div>
        							</div>
                                </div>
                            </div> 
                            
                        </div>
                    </div>
				</div>
              </div>
              
			  <a id="Nieuws"></a>
              <div class="contentsection gray">
                    <div class="mcontent">
						<div class="inner clearfix" style="text-align: center;"> 
                           
							<h1 class="text-center">Lokaal nieuws</h1>
							<div class="divider-empty"></div>
							<div class="fb-page" data-href="
													<?php if ( $store->id == 4 ) { ?>
													https://www.facebook.com/poulepoulette.brasschaat.bredabaan
													<?php } ?>
													<?php if ( $store->id == 5 ) { ?>
													https://www.facebook.com/poulepoulette.antwerpen.sintjansvliet
													<?php } ?>
													<?php if ( $store->id == 6 ) { ?>
													https://www.facebook.com/poulepoulette.antwerpen.wijngaardbrug
													<?php } ?>
								 " data-width="500" data-height="1000" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/poulepoulette.brasschaat.bredabaan"><a href="https://www.facebook.com/poulepoulette.brasschaat.bredabaan">Poule &amp; Poulette Brasschaat Bredabaan 271</a></blockquote></div></div>
                            
                        </div>
                    </div> 
                </div>