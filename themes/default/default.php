<?php
$description = "Poule &amp; Poulette";
if($this->config->item('article_description'))
{
	$description = $this->config->item('article_description');
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Poule &amp; Poulette</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, maximum-scale=1">
		<meta name="description" content="<?= $description ?>">
		
		<link href='http://fonts.googleapis.com/css?family=Alegreya:400,700,900' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{url}css/flexslider.css?l=<?= time() ?>" type="text/css" media="screen" />
        <link href="{url}css/jquery.datetimepicker.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" href="{url}css/styles.css?l=<?= time() ?>" type="text/css">
		<link rel="stylesheet" href="{url}css/layout.css?l=<?= time() ?>" type="text/css">
		
		<link rel="shortcut icon" type="image/png" href="{url}images/favicon.png?l=<?= time() ?>">
		
		<?php $this->layout->partial("platforms/pharma/views/manager_head", TRUE, $content_data); ?>
		<?php $this->layout->partial("platforms/black/views/manager_head", TRUE, $content_data); ?>
	</head>

	<body class="">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.4&appId=1375389439430240";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		
		<?php $this->layout->partial("platforms/pharma/views/manager_bar", TRUE, $content_data); ?>
		<div id="outer">
			
		<div class="widget-area-controls">
            		<ul class="top-controls">
            			<li class="menu-toggle"><a href="#" id="expand"><i class="fa fa-reorder"></i></a></li>
            			<li class="home"><a href="{url}"><i class="fa fa-home"></i></a></li>
            		</ul>
					<ul class="navigation-controls scrolling-navigation" style="margin-top: 100px;">
						<li class="nav-previous"><a href="#" title="Previous Post"><i class="fa fa-angle-up"></i></a></li>
						<li class="nav-next"><a href="#" title="Next Post"><i class="fa fa-angle-down"></i></a></li>
					</ul>
            
            		
            		<div class="menu-social">
                        <ul id="menu-social-links" class="menu">
                            <li><a href="https://www.facebook.com/poulepoulette.belgium" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li>
								<script type="text/javascript" language="javascript">
								{ coded = "iYeJb@jHy2gjHy2gppg.CHe"
								  key = "APrMY5jeniCNlZGBDvWUf6txOkEoTugbJXR2qSIs7h0dVpmFacH1y83L4K9Qwz"
								  shift=coded.length
								  link=""
								  for (i=0; i<coded.length; i++) {
									if (key.indexOf(coded.charAt(i))==-1) {
									  ltr = coded.charAt(i)
									  link += (ltr)
									}
									else {     
									  ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length
									  link += (key.charAt(ltr))
									}
								  }
								document.write("<a href='mailto:"+link+"'><i class='fa fa-envelope'></i></a></li>")
								}
								</script>
                        </ul>
                    </div>	
            </div>
            
            <div id="secondary" class="widget-area" role="complementary">
        
        		<ul>
                    <li><a href="{url}">Home</a></li>
                    <li><a href="{url}kaart">Menukaart</a></li>
                    <li><a href="{url}nieuws">Nieuws</a></li>
                    <li><a href="{url}reserveer">Reserveer</a></li>
                    <li><a href="{url}poulepoulette">Poule &amp; Poulette</a></li>
                    <li><a href="{url}jobs">Jobs</a></li>
                    <li><a href="{url}franchise">Franchise?</a></li>
                </ul>
                    
            </div>
            
			<a id="Top"></a>
			
			<div id="maincontent">
				
				<?php $this->layout->partial($layout_content, TRUE, $content_data); ?>	
				
				<div id="footer">
					<div class="mcontent">
						<p><span class="info">&copy; Copyright Poule &amp; Poulette 2015</span> <span class="info">Wijngaardbrug 8, 2000 Antwerpen</span> <span class="info" style="color: darkgreen;">website gemaakt op 100% recycleerbare pixels door <a href="{url}login">Cubro</a></span>
							<span class="socialbuttons">
								<a href="https://www.facebook.com/poulepoulette.belgium"><img src="{url}images/facebook.png" alt="socialmediaicon"></a>
							</span>
						</p>
					</div>
				</div>
				<a href="#Top" class="snwchanchor"><img src="{url}images/uparrow.png" id="uparrow" width="50" height="50" alt="arrow"></a>
			</div>
		</div>
		<script type="text/javascript">
			var siteurl = "{url}";
		</script>
		<?php $this->layout->partial("platforms/pharma/views/manager_footer", TRUE, $content_data); ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDc38iQDL4EDWPA4tZWlvRxX0GNkpszvvk&amp;sensor=false"></script>
		<script src="{url}js/jqueryeasing.js"></script>
		<script defer src="{url}js/jquery.flexslider-min.js"></script>
        <script src="{url}js/jquery.fullscreenslides.js"></script>
		<script src="{url}js/jquery.datetimepicker.js"></script>
		<script src="{url}js/site.js?l=<?= time() ?>"></script>
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-7888834-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	</body>
</html>